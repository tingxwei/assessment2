const express = require('express');
const mysql = require('mysql');
const app = express();
const cors = require('cors');

const pool = mysql.createPool({
    host: 'localhost', port: 3306,
    user: 'root', password: 'abcd1234',
    database: 'library', connectionLimit: 5,
});

const NODE_PORT = process.argv[2] || 4000;
app.use(cors());

app.get('/books',(req, res) => {
   
    const t = `%${req.query.title}%`;
    const a = `%${req.query.author}%`;
    const o = parseInt(req.query.offset) || 0;
    const l = parseInt(req.query.limit) || 10;
    pool.getConnection(
        (err, conn) => {
            if (err) {
                res.status(404).json({error: err});
                console.log('error')
                return;
            }

            conn.query(
                'select * from books where title like ? OR (CONCAT(author_firstname, author_lastname) LIKE ?) order by title asc ' , // limit ? and offset ? geenrates an error zzzzz
                [t, a,], 
                (err, result) => { 
                    try {
                        if (err) { //check if we have an error
                            res.status(500).json({error: err});
                        } else {
                            //See if result length is 0
                            if (result.length)
                                res.status(200).json(result);
                            else
                                res.status(404).json(
                                    {message: `No books with title ${req.query.q1} or author named ${req.query.q2}`}
                                );
                        }
                    } finally {
                        conn.release();
                    }
                }
            )
        }
    );
});



app.get('/books/:id', (req,res) =>{
    console.log(`${req.params.id}`);
    pool.getConnection((error, conn) => {
        if (error) {
            res.status(500).json({error: error}); return;
        }
        conn.query('select * from books where id = ?', [ req.params.id],
            (error, result) => {
                try {
                    if (error) {
                        res.status(400).json({error: error}); 
                        return;
                    }
                    if (result.length)
                        res.status(200).json(result[0]);
                    else
                        res.status(404).json({error: 'Not Found'});
                } finally { conn.release() ;}
            }
        )
    })
});





app.use(express.static(__dirname + '/public'));

app.get('/media', express.static(__dirname + "/media"));


app.listen(NODE_PORT, () => {
    console.log('Application has started on port %d', NODE_PORT);
    //Ping database
    pool.getConnection((err, conn) => {
        if (err) {
            console.error('Cannot get database connection');
            process.exit(-1);
        }
        conn.ping((err) => {
            try {
                if (err) {
                    console.log('Cannot ping database')
                    process.exit(-1);
                } else
                    console.log('Database is up');
            } finally {
                conn.release();
            }
        })
    });
});