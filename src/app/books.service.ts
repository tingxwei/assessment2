import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';


import 'rxjs/add/operator/take';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class BooksService {

  constructor(private http: HttpClient) { }


  getBooks(config): Promise<any> {
    //Set the query string
    console.log(config.title);
    console.log(config.author);
    console.log(config.offset);
    console.log(config.limit);
    
    let qs = new HttpParams();
    qs = qs.set('title', config.title)
      .set('author', config.author)
      .set('offset', config.offset || 0)
      .set('limit', config.limit || 10 )
      ;

    return (
      this.http.get("http://localhost:4000/books", { params: qs })
        .take(1).toPromise()
    );
  }

  
    getBook(id: number): Promise<any> {
      return (
        this.http.get(`http://localhost:4000/books/${id}`)
          .take(1).toPromise()
      );
    }
  
    
}
