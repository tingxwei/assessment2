import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'

import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';

import { BooksService } from './books.service'
import { AppComponent } from './app.component';
import { BooksComponent } from './books/books.component';

import { BookdetailComponent } from './bookdetail/bookdetail.component';

const routes = [
  { path: '', component: BooksComponent },
  { path: 'books', component: BooksComponent },
  { path: 'books/:id', component: BookdetailComponent },
  { path: '**', redirectTo: '/', pathMatch: 'full'}
]
@NgModule({
  declarations: [
    AppComponent,
    BooksComponent,
    BookdetailComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    FormsModule,
    
  ],
  providers: [BooksService],
  bootstrap: [AppComponent]
})
export class AppModule { }
