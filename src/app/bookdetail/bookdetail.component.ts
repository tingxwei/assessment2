import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BooksService } from '../books.service';
import { Subscription } from 'rxjs/Subscription';
@Component({
  selector: 'app-bookdetail',
  templateUrl: './bookdetail.component.html',
  styleUrls: ['./bookdetail.component.css']
})
export class BookdetailComponent implements OnInit {

  book = {};
  id = 0;
  private id$: Subscription;
  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private BSvc: BooksService) { }

    ngOnInit() {
      
     
      this.id$ = this.activatedRoute.params.subscribe(
        (param) => {
          console.log('> param  = ', param);
          this.id = parseInt(param.id);
          this.BSvc.getBook(this.id)
            .then((result) => this.book = result)
            .catch((error) => {
              alert(`Error: ${JSON.stringify(error)}`);
            });
        }
      )
    }

}
