import { Component, OnInit } from '@angular/core';
import { BooksService } from '../books.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  offset = 0;
  private limit = 10;
  books = [];
  title ='';
  author='';
  constructor(private BSvc: BooksService, private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    
  }

  submit() {
    console.log(this.title);
    console.log(this.author);
    
    this.BSvc.getBooks({ title: this.title, author: this.author, limit: this.limit, offset: this.offset,  })
      .then((result) => { this.books = result })
      .catch((error) => { console.error(error); });
  }

  next() {
    this.offset = this.offset + this.limit
    this.BSvc.getBooks({ title: this.title, author: this.author, limit: this.limit, offset: this.offset, })
      .then((result) => { this.books = result })
      .catch((error) => { console.error(error); });
  }
  prev() {
    this.offset = this.offset - this.limit
    this.BSvc.getBooks({ title: this.title, author: this.author, limit: this.limit, offset: this.offset,  })
      .then((result) => { this.books = result })
      .catch((error) => { console.error(error); });
  }
  showDetails(id: number) {
    console.log('> BookId: %d', id);
    this.router.navigate([ '/books', id ]);
  }
}

